<?php

/***********************************************************************
 * inserts a notice 
 */
function noticeapi_notice_del($module,$module_type,$source_id,$prime_id){

	$SQL = "DELETE FROM {notice_board}
			WHERE module = '%s' AND module_type = '%s'
				AND source_uid = %d AND prime_id = '%d'
	";
	db_query($SQL,$module,$module_type,$source_id,$prime_id);

}
/***********************************************************************
 * inserts a notice 
 */
function noticeapi_notice_add($module,$value_array){
								

	$SQL = "INSERT INTO {notice_board}
			(nb_type,module,module_type,source_uid,end_id,prime_id,
				operation,timestamp)
			VALUES
			(%d,'%s','%s',%d,%d,%d,'%s',%d)
	";
	db_query($SQL,$value_array['nb_type'],$module,
				$value_array['module_type'],$value_array['source_uid'],
				$value_array['dest_id'],$value_array['prime_id'],
				$value_array['op'],time());

}
/***********************************************************************
 * return the main-feed of $uid
 * this means find all new that uid created and all news from 
 * uid's friends.
 */
function noticeapi_notice_feed($uid,$limit=10){
global $user;

	$SQL = "SELECT * FROM {notice_board}
			WHERE 
					 source_uid IN (SELECT fid FROM {friend} WHERE uid=%d)

			ORDER BY timestamp DESC
	";
    $result = db_query_range($SQL,$uid,$user->uid,0,$limit);
	$prev_date = "";
    $output .= "<div id=\"notice_mini_box\">";
    while($record = db_fetch_object($result)){
			$date = date("F j",$record->timestamp);
		if ($date != $prev_date){
			$output .= "<div class=\"notice_mini_header\">$date</div>";
			$prev_date = $date;
		};
        $output .= "<p>"._notice_translate_board($record,'full')."</p>";
    };
    $output .= "</div>";

    return $output;

};
/***********************************************************************
 * return the mini-feed of $uid
 */
function noticeapi_mini_feed($uid,$limit=10){

// find news about user and user's friends
	$SQL = "SELECT * FROM {notice_board}
			WHERE source_uid = %d 
			ORDER BY timestamp DESC
	";
	$output .= '<div id="notice-block-wrapper">';
	$output .= '<div class="notice-mini-header">Mini-Feed</div>';

    $output .= "<div id=\"notice_mini_box\">";
    $result = db_query_range($SQL,$uid,0,$limit);
	$prev_date = "";
    while($record = db_fetch_object($result)){
			$date = date("F j",$record->timestamp);
		if ($date != $prev_date){
			$output .= "<div class=\"notice_mini_header\">$date</div>";
			$prev_date = $date;
		};
        $output .= "<p>"._notice_translate_board($record,'mini')."</p>";
    };
    $output .= "</div>";

	$output .= '</div>';

    return $output;

};
